//=================animation==============

function reveal() {
  var reveals = document.querySelectorAll(".reveal");
  for (var i = 0; i < reveals.length; i++) {
    var windowHeight = window.innerHeight;
    var elementTop = reveals[i].getBoundingClientRect().top;
    var elementVisible = 20;
    if (elementTop < windowHeight - elementVisible) {
      reveals[i].classList.add("active");
    } 
    // else {
    //   reveals[i].classList.remove("active");
    // }
  }
}
window.addEventListener("scroll", reveal);






$(window).on('scroll', function() {
  if(!window.matchMedia('(min-width: 992px)').matches){
 
    document.querySelectorAll(".active-hover-sm").forEach(function(el,idx){
      if(el.classList.contains("run-only-one")){
        return;
        }
  
      var windowHeight = window.innerHeight;
      var elementTop = el.getBoundingClientRect().top;
      var elementVisible = 20;
      if (elementTop < windowHeight - elementVisible) {
        el.classList.add("active");
        el.classList.add("run-only-one");
        setTimeout(() => {
          console.log(el);
          el.classList.remove("active");
  
        }, 2000);
      } 
    });
     
  }
});


 // Active link switching

// ScrollReveal({ reset: true });

// ScrollReveal().reveal(".title", {
//   duration: 1000,
//   origin: "top",
//   distance: "400px",
//   easing: "cubic-bezier(0.5, 0, 0, 1)",
//   rotate: {
//     x: 20,
//     z: -5
//   }
// });

// ScrollReveal().reveal(".fade-in", {
//   duration: 2000,
//   move: 0
// });

// ScrollReveal().reveal(".fade-in3", {
//   duration: 3500,
//   move: 0
// });
// ScrollReveal().reveal(".fade-in2", {
//   duration: 5500,
//   move: 0
// });



// ScrollReveal().reveal(".flip", {
//   delay: 500,
//   duration: 5500,
//   rotate: {
//     x: 30,
//     z: 30
//   }
// });

// ScrollReveal().reveal(".slide-right", {
//   duration: 3000,
//   origin: "left",
//   distance: "300px",
//   easing: "ease-in-out"
// });


// ScrollReveal().reveal(".slide-up", {
//   duration: 2500,
//   origin: "bottom",
//   distance: "100px",
//   easing: "cubic-bezier(.37,.01,.74,1)",
//   opacity: 0.3,
//   scale: 0.5
// });
// ScrollReveal().reveal(".scaleUp", {
//   duration: 4500,
//   scale: 0.85
// });
// ScrollReveal().reveal(".scaleUp2", {
//   duration: 4500,
//   scale: 0.85
// });



// ============================sticky header=============================
$(document).ready(function () {
  if ($("#not-sticky").length != 0) {
    return;
  }
  var $sticky = $("header");
  if ($sticky.length === 0) return;
  var stickyOffsetTop = $sticky.offset().top;

  $(window).scroll(function (e) {
    e.preventDefault();

    var scrollTop = $(window).scrollTop();

    if (scrollTop > stickyOffsetTop) {
      $sticky.addClass("sticky-navbar");
    } else {
      $sticky.removeClass("sticky-navbar");
    }
  });
});

//==================================plus======================
$(".frame-i").on("click", function () {
  $(".overlay-contact, .frame-i").toggleClass("active");

  return false;
});
$(".frame-i2").on("click", function () {
  $(".overlay-contact2, .frame-i2").toggleClass("active");

  return false;
});




//===========================hover========================
// let current = $(".replace-children .replaceable:last-child")
// $(".replace-children .replaceable").hover(function(){
//   const that = $(this)
//   console.log(that);

//   const currentClass = that.attr('class')
//   const nextClass = current.attr('class')
//   if(currentClass == nextClass){
//       return;
//   }
//   that.attr('class',nextClass)
//   current.attr('class',currentClass)
  // firstOldSibling.css('transform',currentTransform)
  // $(this).css('transform',nextTransform)
  
//   $(".replace-children .replaceable").css('z-index',1)
//   current.css('z-index',3)
//   that.css('z-index',2)
//   current = that
// })



//=================================swiper======================

var sliderThumbnail = new Swiper('.slider-thumbnail', {
  slidesPerView: 'auto',
  // centeredSlides: true,
  freeMode: true,
  
  slideToClickedSlide: true,
  watchSlidesVisibility: true,
  watchSlidesProgress: true,

  spaceBetween: 10,

});


var slider = new Swiper('.slider', {

  breakpoints: {
    640: {
        slidesPerView: 1,

    },
   767: {
    slidesPerView: 1.2,

    },
    992: {
      slidesPerView: 1.3,
    },
},
  centeredSlides: true,
  paginationClickable: true,
  loop: true,
  spaceBetween: 30,
  slideToClickedSlide: true,
  // mousewheel: 
  // {
  //   invert: true,
  // },

  // autoplay: 
  // {
  //   delay: 2000,
  // },

  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
  thumbs: {
    swiper: sliderThumbnail
  },


});




